module.exports = {
  root: true,
  // 脚本在执行期间访问的额外的全局变量
  env: {
    node: true,
    browser: true
  },
  extends: [
    // plugin:(此处不能有空格)包名/配置名称。
    // 解析时plugin是解析成 eslint - plugin - vue。
    // 如果有空格会解析失败，eslint-plugin-vue。
    // plugin可以省略包名的前缀 eslint-plugin-
    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/prettier'
  ],
  // 指定解析器
  // babel-ESLint: 一个对Babel解析器的包装，使其能够与ESLint兼容。
  // parser: 'babel-eslint',
  // 设置解析器能帮助ESLint确定什么是解析错误。
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  /**
   * 每个规则有【3】个错误级别。
   * off或0: 关闭该规则；
   * warn或1: 开启规则，使用警告级别的错误，不会导致程序退出；
   * error或2: 开启规则，使用错误级别的错误，当被触发的时候，程序会退出。
   */
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': 'off',
    'no-useless-escape': 'off',
    'no-undef': 'off',
    'no-constant-condition': 'off'
    // add your custom rules here
    // it is base on https://github.com/vuejs/eslint-config-vue
  }
}
