import request from '@/utils/request'

// 校验
export function getUploadStatus(data) {
  return request({
    url: 'api/file/part/check',
    method: 'get',
    params: data
  })
}

// 上传
export function sliceUpload(data) {
  return request({
    url: 'api/file/part/upload',
    method: 'post',
    data
  })
}

// 合并
export function mergeUpload(data) {
  return request({
    url: 'api/file/part/merge',
    method: 'get',
    params: data
  })
}

export default { getUploadStatus, sliceUpload, mergeUpload }
