import Vue from 'vue'
// icon按需加载
import 'vue-awesome/icons/brands/weixin'
import 'vue-awesome/icons/brands/weibo'
import 'vue-awesome/icons/brands/telegram'
import 'vue-awesome/icons/brands/twitter'
import 'vue-awesome/icons/brands/medium'
import 'vue-awesome/icons/brands/github'
import 'vue-awesome/icons/envelope'

import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)
