// 场景：clickOutside 自定义指令可以应用于需要在点击元素外部时触发某些操作的场景，例如：

// 点击外部关闭弹窗：当用户点击弹窗外部时，需要关闭弹窗并执行一些操作，例如清空输入框、重置表单等。
// 点击外部隐藏下拉菜单：当用户点击下拉菜单外部时，需要隐藏下拉菜单并执行一些操作，例如清空搜索框、重置筛选条件等。
// 点击外部取消选中状态：当用户点击选中元素外部时，需要取消选中状态并执行一些操作，例如清空选中项、重置状态等。
// 实现原理：clickOutside 自定义指令的实现原理是通过 document 对象的 click 事件来监听用户的点击行为，当用户点击元素外部时，会触发 click 事件，从而执行自定义指令绑定的回调函数。
// 总之，clickOutside 自定义指令可以帮助我们实现一些常见的交互需求，提升用户体验和操作效率。
const clickOutside = {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      if (!(el === event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event)
      }
    }
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  }
}

export default clickOutside
