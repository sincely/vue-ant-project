// 场景： copy 自定义指令可以应用于需要实现一键复制文本内容的场景，例如：

// 复制分享链接：当用户点击分享按钮时，需要将当前页面的分享链接复制到剪贴板中，方便用户分享给其他人。
// 复制优惠码：当用户点击领取优惠券按钮时，需要将优惠码复制到剪贴板中，方便用户在购物时使用。
// 复制代码片段：当用户需要复制代码片段时，可以通过点击复制按钮，将代码片段复制到剪贴板中，方便用户在编辑器中粘贴使用。

// 总之，copy 自定义指令可以帮助我们实现一些常见的复制操作，提升用户体验和操作效率。

const copy = {
  bind: function (el, binding) {
    el.addEventListener('click', function () {
      const textToCopy = binding.value
      const input = document.createElement('input')
      input.setAttribute('value', textToCopy)
      document.body.appendChild(input)
      input.select()
      document.execCommand('copy')
      document.body.removeChild(input)
    })
  }
}

export default copy
