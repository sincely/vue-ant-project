// 场景： fit-columns 自定义指令可以应用于需要自动调整表格列宽的场景，例如：

// 数据展示：当我们需要在页面上展示大量数据时，可以使用表格进行展示，通过 fit-columns 自定义指令可以自动调整表格列宽，使得数据更加清晰易读。
// 数据编辑：当我们需要在页面上编辑表格数据时，可以使用表格进行编辑，通过 fit-columns 自定义指令可以自动调整表格列宽，使得编辑更加方便快捷。
// 数据导出：当我们需要将表格数据导出为 Excel 或 CSV 格式时，可以使用表格进行导出，通过 fit-columns 自定义指令可以自动调整表格列宽，使得导出的数据更加美观。

// 总之，fit-columns 自定义指令可以帮助我们实现自动调整表格列宽的功能，提升数据展示、编辑和导出的效率和美观度。
import './fit-columns.less'

function adjustColumnWidth(table, padding = 0) {
  const colgroup = table.querySelector('colgroup')
  const colDefs = [...colgroup.querySelectorAll('col')]
  colDefs.forEach((col) => {
    const clsName = col.getAttribute('name')
    const clsWidth = col.getAttribute('width')
    if (clsWidth < 200) return
    const cells = [...table.querySelectorAll(`td.${clsName}`), ...table.querySelectorAll(`th.${clsName}`)]
    if (cells[0] && cells[0].classList && cells[0].classList.contains && cells[0].classList.contains('leave-alone')) {
      return
    }
    const widthList = cells.map((el) => {
      return (el.querySelector('.cell') && el.querySelector('.cell').scrollWidth) || 0
    })
    const max = Math.max(...widthList)
    table.querySelectorAll(`col[name=${clsName}]`).forEach((el) => {
      // console.log(222, max + padding)
      el.setAttribute('width', max + padding > 500 ? 500 : max + padding)
    })
  })
}

const fitColumns = {
  update() {},
  bind() {},
  inserted(el, binding) {
    setTimeout(() => {
      adjustColumnWidth(el, binding.value)
    }, 300)
  },
  componentUpdated(el, binding) {
    el.classList.add('r-table')
    setTimeout(() => {
      adjustColumnWidth(el, binding.value)
    }, 300)
  },
  unbind() {}
}

export default fitColumns
