import Vue from 'vue'
import empty from './empty'
import format from './format'
import screenfull from './screenfull'
import badge from './badge'
import debounce from './debounce'
import drag from './drag'
import fitColumns from './fit-columns'
import enterToInput from './enter-to-input'
import resizeHeight from './resize-height'
import resizeWidth from './resize-width'
import inputFilter from './input-filter'
import copy from './copy'
import longpress from './longpress'
import clickOutside from './click-outside'

// import other directives
const directives = {
  empty,
  format,
  screenfull,
  badge,
  debounce,
  drag,
  fitColumns,
  enterToInput,
  resizeHeight,
  resizeWidth,
  inputFilter,
  copy,
  longpress,
  clickOutside
  // other directives
}

Object.keys(directives).forEach((name) => Vue.directive(name, directives[name]))
