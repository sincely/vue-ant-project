// 场景： input-filter 自定义指令可以应用于需要对用户输入进行过滤和限制的场景，例如：

// 输入框过滤：当我们需要在输入框中输入特定类型的数据时，可以使用 input-filter 自定义指令对用户输入进行过滤和限制，例如只允许输入数字、字母或特定字符等。
// 表单验证：当我们需要对表单中的数据进行验证时，可以使用 input-filter 自定义指令对用户输入进行过滤和限制，例如验证手机号码、邮箱地址等。
// 密码输入：当我们需要用户输入密码时，可以使用 input-filter 自定义指令对用户输入进行过滤和限制，例如限制密码长度、只允许输入特定字符等。

// 总之，input-filter 自定义指令可以帮助我们实现对用户输入进行过滤和限制的功能，提升表单验证和数据输入的效率和准确性。

const findEle = (parent, type) => {
  return parent.tagName.toLowerCase() === type ? parent : parent.querySelector(type)
}

const trigger = (el, type) => {
  const e = document.createEvent('HTMLEvents')
  e.initEvent(type, true, true)
  el.dispatchEvent(e)
}

const inputFilter = {
  mounted(el, binding, vnode) {
    const bindV = binding.value
    const regRule = bindV.regRule ? bindV.regRule : /[^\a-zA-Z0-9\u4E00-\u9FA5]+$/g
    const length = bindV.length ? bindV.length : 30
    const $inp = findEle(el, 'input')
    el.$inp = $inp
    $inp.handle = () => {
      const val = $inp.value
      $inp.value = val.replace(regRule, '').substring(0, length)

      trigger($inp, 'input')
    }
    $inp.addEventListener('keyup', $inp.handle)
  },
  unmounted(el) {
    el.$inp.removeEventListener('keyup', el.$inp.handle)
  }
}

export default inputFilter
