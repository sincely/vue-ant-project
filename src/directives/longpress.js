// 场景： longpress 自定义指令可以应用于需要长按触发事件的场景，例如：
// 按钮长按：当我们需要在按钮上长按触发某个事件时，可以使用 longpress 自定义指令，例如长按删除按钮可以删除某个元素。
// 图片预览：当我们需要在图片上长按触发预览事件时，可以使用 longpress 自定义指令，例如长按图片可以弹出预览框。
// 列表操作：当我们需要在列表中长按触发某个操作时，可以使用 longpress 自定义指令，例如长按列表项可以弹出操作菜单。
// 总之，longpress 自定义指令可以帮助我们实现长按触发事件的功能，提升用户体验和操作效率。

// 在 bind 钩子函数中绑定了 mousedown、touchstart、click、mouseout、touchend 和 touchcancel 事件。
// 当用户按下鼠标或触摸屏时，我们会启动一个定时器，如果在指定的时间内没有松开鼠标或手指，则执行指令的回调函数。
// 如果用户在指定的时间内松开了鼠标或手指，则取消定时器。
const longpress = {
  bind: function (el, binding) {
    let pressTimer = null
    const duration = binding.value || 500 // 默认长按时间为 500ms

    const start = function (event) {
      if (event.type === 'click' && event.button !== 0) {
        return
      }

      if (pressTimer === null) {
        pressTimer = setTimeout(() => {
          handler()
        }, duration)
      }
    }

    const cancel = function () {
      if (pressTimer !== null) {
        clearTimeout(pressTimer)
        pressTimer = null
      }
    }

    const handler = function () {
      binding.value()
    }

    el.addEventListener('mousedown', start)
    el.addEventListener('touchstart', start)

    el.addEventListener('click', cancel)
    el.addEventListener('mouseout', cancel)
    el.addEventListener('touchend', cancel)
    el.addEventListener('touchcancel', cancel)
  }
}

export default longpress
