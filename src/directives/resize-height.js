// 场景： resize-height 自定义指令可以应用于需要根据内容自适应高度的场景，例如：

// 文本框自适应高度：当我们需要在文本框中输入多行文本时，可以使用 resize-height 自定义指令，使文本框根据内容自适应高度，避免内容溢出或留白。
// 评论框自适应高度：当我们需要在评论框中输入多行文本时，可以使用 resize-height 自定义指令，使评论框根据内容自适应高度，提升用户体验和操作效率。
// 动态列表自适应高度：当我们需要在动态列表中展示不同高度的内容时，可以使用 resize-height 自定义指令，使列表项根据内容自适应高度，避免内容溢出或留白。

// 总之，resize - height 自定义指令可以帮助我们实现根据内容自适应高度的功能，提升用户体验和界面美观度。

const resizeHeight = {
  // 绑定时调用
  bind(el, binding) {
    let height = ''
    function isResize() {
      // 可根据需求，调整内部代码，利用 binding.value 返回即可
      const style = document.defaultView.getComputedStyle(el)
      if (height !== style.height) {
        // 此处关键代码，通过此处代码将数据进行返回，从而做到自适应
        binding.value({ height: style.height })
      }
      height = style.height
    }
    // 设置调用函数的延时，间隔过短会消耗过多资源
    el.__vueSetInterval__ = setInterval(isResize, 100)
  },
  unbind(el) {
    clearInterval(el.__vueSetInterval__)
  }
}

export default resizeHeight
