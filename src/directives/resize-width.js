// 场景： resize-width 自定义指令可以应用于需要根据内容自适应宽度的场景，例如：

// 图片自适应宽度：当我们需要在页面中展示不同宽度的图片时，可以使用 resize-width 自定义指令，使图片根据内容自适应宽度，避免图片变形或溢出。
// 表格自适应宽度：当我们需要在页面中展示不同宽度的表格时，可以使用 resize-width 自定义指令，使表格根据内容自适应宽度，避免表格变形或溢出。
// 动态列表自适应宽度：当我们需要在动态列表中展示不同宽度的内容时，可以使用 resize-width 自定义指令，使列表项根据内容自适应宽度，避免内容变形或溢出。

// 总之，resize-width 自定义指令可以帮助我们实现根据内容自适应宽度的功能，提升用户体验和界面美观度。

const resizeWidth = {
  // 绑定时调用
  bind(el, binding) {
    let width = ''
    function isResize() {
      // 可根据需求，调整内部代码，利用binding.value返回即可
      const style = document.defaultView.getComputedStyle(el)
      if (width !== style.width) {
        // 此处关键代码，通过此处代码将数据进行返回，从而做到自适应
        binding.value({ width: style.width })
      }
      width = style.width
    }
    // 设置调用函数的延时，间隔过短会消耗过多资源
    el.__vueSetInterval__ = setInterval(isResize, 100)
  },
  unbind(el) {
    clearInterval(el.__vueSetInterval__)
  }
}

export default resizeWidth
