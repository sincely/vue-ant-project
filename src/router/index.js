import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    // 登录
    path: '/login/demo01',
    name: 'demo01',
    component: () => import(/* webpackChunkName: "demo01" */ '@/views/login/demo-01')
  },
  {
    path: '/login/demo02',
    name: 'demo02',
    component: () => import(/* webpackChunkName: "demo02" */ '@/views/login/demo-02')
  },
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '@/views/home')
  },
  {
    path: '/pieChart',
    name: 'pieChart',
    component: () => import(/* webpackChunkName: "pieChart" */ '@/views/pieChart')
  },
  {
    path: '/barChart',
    name: 'barChart',
    component: () => import(/* webpackChunkName: "barChart" */ '@/views/barChart')
  },
  {
    path: '/print',
    name: 'print',
    component: () => import(/* webpackChunkName: "print" */ '@/views/print')
  },
  {
    path: '/vue-html2pdf',
    name: 'vue-html2pdf',
    component: () => import(/* webpackChunkName: "vue-html2pdf" */ '@/views/vue-html2pdf')
  },
  {
    path: '/htmlToPdf',
    name: 'htmlToPdf',
    component: () => import(/* webpackChunkName: "htmlToPdf" */ '@/views/htmlToPdf')
  },
  {
    path: '/process',
    name: 'process',
    component: () => import(/* webpackChunkName: "process" */ '@/views/process')
  },
  {
    path: '/flexContainer',
    name: 'flexContainer',
    component: () => import(/* webpackChunkName: "flexContainer" */ '@/views/flexContainer')
  },
  {
    path: '/lottie',
    name: 'lottie',
    component: () => import(/* webpackChunkName: "lottie" */ '@/views/lottie')
  },
  {
    path: '/animate',
    name: 'animate',
    component: () => import(/* webpackChunkName: "animate" */ '@/views/animate')
  },
  {
    path: '/anonymitySlot',
    name: 'anonymitySlot', // 匿名插槽
    component: () => import(/* webpackChunkName: "anonymitySlot" */ '@/views/anonymitySlot/father')
  },
  {
    path: '/defaultSlot',
    name: 'defaultSlot', // 匿名插槽
    component: () => import(/* webpackChunkName: "defaultSlot" */ '@/views/defaultSlot/father')
  },
  {
    path: '/fullNameSlot',
    name: 'fullNameSlot', // 具名插槽
    component: () => import(/* webpackChunkName: "fullNameSlot" */ '@/views/fullNameSlot/father')
  },
  {
    path: '/scopeSlot',
    name: 'scopeSlot', // 作用域插槽
    component: () => import(/* webpackChunkName: "scopeSlot" */ '@/views/scopeSlot/father')
  },
  {
    path: '/editablecell',
    name: 'editablecell', // 可编辑表格
    component: () => import(/* webpackChunkName: "editablecell" */ '@/views/editablecell')
  },
  {
    path: '/complexTable',
    name: 'complexTable', // 高级表格
    component: () => import(/* webpackChunkName: "complexTable" */ '@/views/complexTable')
  },
  {
    path: '/dragModal',
    name: 'dragModal', // 拖拽弹窗
    component: () => import(/* webpackChunkName: "dragModal" */ '@/views/dragModal')
  },
  {
    path: '/screenFullModal',
    name: 'screenFullModal', // 全屏
    component: () => import(/* webpackChunkName: "screenFullModal" */ '@/views/screenFullModal')
  }
]

const router = new VueRouter({
  mode: 'hash',
  scrollBehavior(to, from, savedPosition) {
    console.log(to, from, savedPosition)
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes
})

export default router
