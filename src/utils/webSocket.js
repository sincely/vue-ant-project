// 封装原生websocket,支持自动重连
class websocket {
  constructor(url) {
    this.ws = null // websocket实例
    this.timeout = 2000 // 重连间隔
    this.lockReconnect = false //避免重复连接
    this.heartInterval = null // 心跳时器
  }

  // 初始化websocket连接
  initConnect() {
    try {
      if ('WebSocket' in window) {
        this.ws = new WebSocket(url)
        initEventHandle(url)
      } else {
        console.log('您的浏览器不支持websocket')
      }
    } catch (error) {
      reconnect(url)
    }
  }

  // 初始化事件监听
  initEventHandle(url) {
    this.ws.onclose = () => {
      //console.log('websocket服务关闭了');
      reconnect(url)
    }
    this.ws.onerror = () => {
      //console.log('websocket服务异常了');
      reconnect(url)
    }
    this.ws.onopen = () => {
      //console.log('websocket服务连接成功');
      //如果获取到消息，心跳检测重置
      //拿到任何消息都说明当前连接是正常的
      //接受消息后的UI变化
      this.doWithMsg(evnt.data)
      //心跳检测重置
      heartCheck.reset().start()
    }
  }

  // doWithMsg
  doWithMsg(msg) {
    //console.log(msg);
  }

  // 重连
  reconnect(url) {
    if (this.lockReconnect) return
    this.lockReconnect = true

    //没连接上会一直重连，设置延迟避免请求过多
    setTimeout(() => {
      this.initConnect(url)
    }, this.timeout)
  }

  // 心跳检测
  heartCheck() {
    this.heartInterval = setInterval(() => {
      //这里发送一个心跳，后端收到后，返回一个心跳消息，
      //onmessage拿到返回的心跳就说明连接正常
      this.ws.send('HeartBeat')
    }, this.timeout)
  }

  // 重置心跳
  reset() {
    clearInterval(this.heartInterval)
  }
}

export default websocket
