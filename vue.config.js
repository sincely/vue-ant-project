const path = require('path')
const fs = require('fs')
const webpack = require('webpack')
const isProduction = process.env.NODE_ENV !== 'development' // 是否为生产环境
const CompressionWebpackPlugin = require('compression-webpack-plugin') // gzip压缩
const UglifyJsPlugin = require('uglifyjs-webpack-plugin') // 代码压缩
function resolve(dir) {
  return path.join(__dirname, dir)
}
// 生产环境和开发环境要区别开,获取插件plugin配置
function getPlugins() {
  let plugins = []
  let productionGzipExtensions = ['html', 'js', 'css']
  plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/))
  // 生产环境配置
  if (isProduction) {
    // 代码压缩
    plugins.push(
      new UglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: false // 构建时不想出现注释
          },
          warnings: false,
          // 生产环境自动删除console
          compress: {
            drop_debugger: true,
            drop_console: true,
            pure_funcs: ['console.log']
          }
        },
        sourceMap: false, // 是否启用文件缓存
        parallel: 4 // 使用多进程并行运行来提高构建速度
      }),
      // gzip压缩
      new CompressionWebpackPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
        threshold: 10240, // 只有大小大于该值的资源会被处理 10240
        minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
        deleteOriginalAssets: false // 删除原文件
      })
    )
  }
  return plugins
}
module.exports = {
  publicPath: '/',
  productionSourceMap: false,
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: !isProduction,
  configureWebpack: {
    plugins: getPlugins(),
    devtool: isProduction ? false : 'source-map',
    //关闭webpack的性能提示
    performance: {
      hints: false
    }
  },
  devServer: {
    port: 8080,
    open: true,
    https: false,
    inline: true,
    hotOnly: true, // 热更新
    // 让浏览器 overlay 同时显示警告和错误
    overlay: {
      warnings: false,
      errors: true
    }
    // webpack的devServer.before 可以提供了一个在devServer内部的所有中间件执行之前的自定义执行函数。
    // 故：可以采用此方法来模拟后台数据接口。
    // https://www.jianshu.com/p/c4883c04acb3
    // before: require('./mock/mock-server.js'),
  },
  // css配置项支持less scss postcss..
  css: {
    // 提取CSS在开发环境模式下是默认不开启的,因为它和CSS热重载不兼容。
    // 然而，你仍然可以将这个值显性地设置为true在所有情况下都强制提取。
    // 是否使用css分离插件ExtractTextPlugin,默认false能导致css热更新失败
    // extract: true,
    loaderOptions: {
      less: {
        // 如果使用less-loader@5，请移除lessOptions这一级直接配置选项。
        // 每一个lang='less'样式下面引入公共封装样式，无需每个页面再手动引入（less@^6.0.0）
        prependData: `@import "~@/styles/variables.less";`,
        lessOptions: {
          javascriptEnabled: true
        }
      },
      css: {},
      postcss: {},
      scss: {}
    }
  },
  // 默认情况下babel-loader会忽略所有node_modules中的文件。
  // 如果你想要通过Babel显式转译一个依赖，可以在这个选项中列出来。
  transpileDependencies: [],
  chainWebpack: (config) => {
    // 默认开启prefetch(预先加载模块)，
    // 提前获取用户未来可能会访问的内容 在首屏会把这十几个路由文件
    // ，都一口气下载了 所以我们要关闭这个功能模块
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')
    // set svg-sprite-loader
    config.module.rule('svg').exclude.add(resolve('src/icons')).end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
}
